#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "TabelaHash.h" //inclui os Prot�tipos

//Defini��o do tipo Hash
typedef struct HASH{
    int qtd, size;
    ITEM **itens;
}HASH;

HASH* criaHash(int size){
    HASH* ha = (HASH*) malloc(sizeof(HASH));
    if(ha != NULL){
        int i;
        ha->size = size;
        ha->itens = (ITEM**) malloc(size * sizeof(ITEM*));
        if(ha->itens == NULL){
            free(ha);
            return NULL;
        }
        ha->qtd = 0;
        for(i=0; i < ha->size; i++)
            ha->itens[i] = NULL;
    }
    return ha;
}

void liberaHash(HASH* ha){
    if(ha != NULL){
        int i;
        for(i=0; i < ha->size; i++){
            if(ha->itens[i] != NULL)
                free(ha->itens[i]);
        }
        free(ha->itens);
        free(ha);
    }
}

void listarHash(HASH* ha){
    if(ha != NULL){
        int i;
        printf("Listando hash:\n");
        for(i=0; i < ha->size; i++){
            if(ha->itens[i] != NULL)
                printf("%d ", ha->itens[i]->codigo);
        }
        free(ha->itens);
        free(ha);
    }
}

int chaveDivisao(int chave, int size){
    return (chave & 0x7FFFFFFF) % size;
}

int sondagemLinear(int pos, int i, int size){
    return ((pos + i) & 0x7FFFFFFF) % size;
}

int insereHash_EnderAberto(HASH* ha, ITEM item){
    if(ha == NULL || ha->qtd == ha->size)
        return 0;

    int chave = item.codigo;

    int i, pos, newPos;
    pos = chaveDivisao(chave,ha->size);
    for(i=0; i < ha->size; i++){
        newPos = sondagemLinear(pos,i,ha->size);

        if(ha->itens[newPos] == NULL){
            ITEM* novo;
            novo = (ITEM*) malloc(sizeof(ITEM));
            if(novo == NULL)
                return 0;
            *novo = item;
            ha->itens[newPos] = novo;
            ha->qtd++;
            return 1;
        }
    }
    return 0;
}

int buscaHash_EnderAberto(HASH* ha, int cod, ITEM* item){
    if(ha == NULL)
        return 0;

    int i, pos, newPos;
    pos = chaveDivisao(cod, ha->size);
    for(i=0; i < ha->size; i++){
        newPos = sondagemLinear(pos, i, ha->size);
        
        if(ha->itens[newPos] == NULL)
            return 0;

        if(ha->itens[newPos]->codigo == cod){
            *item = *(ha->itens[newPos]);
            return 1;
        }
    }
    return 0;
}
