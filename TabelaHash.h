typedef struct{
    int codigo;
}ITEM;

typedef struct HASH HASH;

HASH* criaHash(int TABLE_SIZE);
void liberaHash(HASH* ha);
void listarHash(HASH* ha);
int insereHash_EnderAberto(HASH* ha, ITEM item);
int buscaHash_EnderAberto(HASH* ha, int cod, ITEM* item);
int chaveDivisao(int chave, int size);
int sondagemLinear(int pos, int i, int size);
