#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "TabelaHash.h"

#define hash_size 10000				//tamanho tabela hash
#define tam 9000					//tamanho vetor de codigos
#define intervalo 30000					//intevalo aleatorio dos codigos

int main(){
	clock_t t_inicio, t_fim;
	srand(time(NULL));
    
	int ret=0;
    HASH *tabela = criaHash(hash_size);
    ITEM itens[tam], itm;

    for(int i=0; i < tam; i++)
    	itens[i].codigo = rand() % intervalo;
	
    t_inicio = clock();
	for(int i=0; i < tam; i++)
        insereHash_EnderAberto(tabela, itens[i]);
    t_fim = clock();
    
    //listarHash(tabela);
	
	printf("\n\n------------------------------------");
	printf("\nTempo de insersao: %lf segundos", t_fim-t_inicio*1000/CLOCKS_PER_SEC);
    printf("\n------------------------------------");
	
	t_inicio = clock();
	for(int i=0; i < tam; i++)
    	ret = buscaHash_EnderAberto(tabela, itens[i].codigo, &itm);
    t_fim = clock();
    
    printf("\nTempo de busca: %lf segundos", t_fim-t_inicio*1000/CLOCKS_PER_SEC);
    printf("\n------------------------------------\n");
    
    liberaHash(tabela);

    return 0;
}
